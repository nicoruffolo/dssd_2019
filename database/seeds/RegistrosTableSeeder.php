<?php

use Illuminate\Database\Seeder;
use App\Unidad;
use App\Registro;
use App\EndPoint;

class RegistrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unidad::truncate();

        $unidad = new Unidad();
        $unidad->nombre = "Lisandro Olmos";
        $unidad->nro_unidad = 1;
        $unidad->coordenada_x = "-34.99879";
        $unidad->coordenada_y = "-58.04139";
        $unidad->save();

        $unidad = new Unidad();
        $unidad->nombre = "Sierra Chica";
        $unidad->nro_unidad = 2;
        $unidad->coordenada_x = "-36.83867";
        $unidad->coordenada_y = "-60.22645";
        $unidad->save();

        $unidad = new Unidad();
        $unidad->nombre = "San Nicolás de Los Arroyos";
        $unidad->nro_unidad = 3;
        $unidad->coordenada_x = "-33.35242";
        $unidad->coordenada_y = "-60.19768";
        $unidad->save();

        $unidad = new Unidad();
        $unidad->nombre = "Bahía Blanca";
        $unidad->nro_unidad = 4;
        $unidad->coordenada_x = "-38.68651";
        $unidad->coordenada_y = "-62.27582";
        $unidad->save();

        $unidad = new Unidad();
        $unidad->nombre = "Villa Floresta";
        $unidad->nro_unidad = 5;
        $unidad->coordenada_x = "-38.686083";
        $unidad->coordenada_y = "-62.275502";
        $unidad->save();

		$unidad = new Unidad();
        $unidad->nombre = "San Cayetano";
        $unidad->nro_unidad = 6;
        $unidad->coordenada_x = "-38.696678";
        $unidad->coordenada_y = "-62.280153";
        $unidad->save();

        Registro::truncate();

        $registro = new Registro();
        $registro->unidad_id = 1;
        $registro->fecha = '2019-12-15';  // Fecha a probar
        $registro->hora = 16;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 2;
        $registro->fecha = '2019-12-12';  // Fecha a probar
        $registro->hora = 14;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 2;
        $registro->fecha = '2019-12-11';
        $registro->hora = 14;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 2;
        $registro->fecha = '2019-12-13';
        $registro->hora = 14;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 2;
        $registro->fecha = '2019-12-10';
        $registro->hora = 14;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 2;
        $registro->fecha = '2019-12-14';
        $registro->hora = 14;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 6;
        $registro->fecha = '2019-12-14';  // Fecha a probar   
        $registro->hora = 17;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 6;
        $registro->fecha = '2019-12-13';
        $registro->hora = 17;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 6;
        $registro->fecha = '2019-12-12';
        $registro->hora = 17;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 3;
        $registro->fecha = '2019-12-20';     //Fecha a probar
        $registro->hora = 12;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 3;
        $registro->fecha = '2019-12-21';
        $registro->hora = 12;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 3;
        $registro->fecha = '2019-12-22';
        $registro->hora = 12;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 3;
        $registro->fecha = '2019-12-19';
        $registro->hora = 12;
        $registro->save();

        $registro = new Registro();
        $registro->unidad_id = 3;
        $registro->fecha = '2019-12-18';
        $registro->hora = 12;
        $registro->save();

        EndPoint::truncate();

        $end = new EndPoint();
        $end->nombre = "/ayuda";
        $end->descripcion = "Retorna un Json con informacion de los diferentes endpoints y la manera de utilizar cada uno.";
        $end->save();

        $end = new EndPoint();
        $end->nombre = "/buscarFecha/{fecha}/{hora}/{unidad}";
        $end->descripcion = "Primero se verifica si la fecha, hora y unidad solicitadas estan disponibles. En caso afirmativo se retorna un mensaje que avisa que las mismas estan disponibles. Por otro lado, si la fecha se encuentra reservada para esa fecha, hora y unidad se busca una fecha proxima a la solicitada (sin cambiar la hora elegida). El orden para buscar la fecha proxima es el siguiente: primero se chequea si las fechas posteriores a la solicitada estan reservadas (las dos fechas siguientes). Si alguna de ellas esta disponible se la retorna en un mensaje. Por otro lado si las dos fechas siguientes tambien se encuentran ocupadas se procede a buscar las fechas anteriores a la solicitada (las dos fechas anteriores). Si alguna de ellas esta disponible se retorna la mas proxima en un mensaje. Por ultimo, si tanto la fecha solicitada como las dos siguientes y las dos anteriores se encuentran reservadas entonces se retorna un mensaje con un aviso para cambiar de unidad. Para consumir este servicio se debe proporcionar una fecha (unicamente en formato 'Y-m-d', por ej: '2019-11-20', la fecha debe ser mayor a la de hoy y no debe superar los dos meses a partir de la fecha de hoy), una hora (un numero natural del 10 al 21, por ej: '11' hace referencia a las 11 am y '15' hace referencia a las 3 pm. Solo se puede buscar y reservar en el rango horario de 10 a 21, es decir de 10 am a 9 pm) y por ultimo una unidad (un numero natural del 1 al 6). Se debe respetar el orden 'fecha/hora/unidad' y el tipo de dato especifico para cada uno de ellos. Caso contrario se retornara un mensaje de error.";
        $end->save();

        $end = new EndPoint();
        $end->nombre = "/unidades";
        $end->descripcion = "Retorna un Json con informacion de las unidades registradas. Actualmente hay 6";
        $end->save();


        $end = new EndPoint();
        $end->nombre = "/reservar/{fecha}/{hora}/{unidad}";
        $end->descripcion = "Permite reservar una fecha y hora para una unidad. Primero se verifica que no haya una reserva ya existente para la fecha, hora y unidad proporcionadas. Si la misma existe se retorna un mensaje avisando que ya existe una reserva. Caso contrario se registra una nueva reserva para la fecha, hora y unidad proporcionados y se retorna un mensaje de exito avisando que se pudo llevar a cabo la reserva. Para consumir este servicio se debe proporcionar una fecha (unicamente en formato 'Y-m-d', por ej: '2019-11-20', la fecha debe ser mayor a la de hoy y no debe superar los dos meses a partir de la fecha de hoy), una hora (un numero natural del 10 al 21, por ej: '11' hace referencia a las 11 am y '15' hace referencia a las 3 pm. Solo se puede buscar y reservar en el rango horario de 10 a 21, es decir de 10 am a 9 pm) y por ultimo una unidad (un numero natural del 1 al 6). Se debe respetar el orden 'fecha/hora/unidad' y el tipo de dato especifico para cada uno de ellos. Caso contrario se retornara un mensaje de error.";
        $end->save();

        $end = new EndPoint();
        $end->nombre = "/buscarUnidadesCercanas/{unidad}";
        $end->descripcion = "Recibe una unidad (un numero natural del 1 al 6) y retorna un Json con las unidades mas cercanas a la recibida";
        $end->save();

    }
}
