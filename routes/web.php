<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/buscarFecha/{fecha}/{hora}/{unidad}', 'Service@disponibilidad');

Route::get('/unidades', 'Service@unidades');

Route::post('/reservar', 'Service@registrar');

Route::get('/reservar', 'Service@devolver');

Route::get('/ayuda', 'Service@ayuda');

Route::get('/buscarUnidadesCercanas/{unidad}', 'Service@buscarUnidades');