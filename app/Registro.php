<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    public function unidad(){ 
      return $this->belongsTo(Unidad::class, 'unidad_id');
  }
}
