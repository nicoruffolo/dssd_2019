<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class Service extends Controller
{

	public function devolver() {
		return redirect('/');
	}

	public function unidades(){

		$unidades =App\Unidad::select('nro_unidad', 'nombre')
                   ->get();
		return response()->json($unidades);
	}


    public function disponibilidad($fecha, $hora, $unidad) {

    	$op = $this->validacion($fecha, $hora, $unidad);

    	if ($op == false){
    		return "Argumentos Incorrectos: Por favor utilize el endpoint '/ayuda' para mas informacion";
    	}

    	// Falta validar los datos de entrada

    		$resultado = $this->validarFechasAproximadas($fecha, $hora, $unidad);
    		if ($resultado == "null") {
    			$json = array("fecha" => "null", "hora" => $hora, "unidad" => $unidad, "valor" => "false");
    			$var = json_encode($json);
    			return $var;
    			
    		}

    		else {
    			$json = array("fecha" => $resultado, "hora" => $hora, "unidad" => $unidad, "valor" => "true");
    			$var = json_encode($json);
    			return $var;
    		}
    	
    }

    public function verificarDisponibilidad ($fecha, $hora, $unidad){

    	$disp = App\Registro::where('fecha', $fecha)->where('hora', $hora)->where('unidad_id', $unidad)->count();
    	return $disp;
    }

    public function validarFechasAproximadas ($fecha, $hora, $unidad) {

    	$fechasig = date("Y-m-d",strtotime($fecha."+ 1 days")); // esto se le suma a la fecha que se elige no al dia de hoy
    	$ok = $this->verificarDisponibilidad($fechasig, $hora, $unidad);
    	if ($ok == 0){
    		return $fechasig;
    	}
    	else{
    		$fechasig = date("Y-m-d",strtotime($fecha."+ 2 days"));
    		$ok = $this->verificarDisponibilidad($fechasig, $hora, $unidad);
    		if ($ok == 0){
    			return $fechasig;
    		}
    		else {
    			$fechasig = date("Y-m-d",strtotime($fecha."- 1 days"));
    			$fecha_actual = date("Y-m-d");
    			if ($fechasig == $fecha_actual) {
    				return "null";
    			}
    			$ok = $this->verificarDisponibilidad($fechasig, $hora, $unidad);
    			if ($ok == 0){
	    			return $fechasig;
    			}
    			else{
    				$fechasig = date("Y-m-d",strtotime($fecha."- 2 days"));
    				$fecha_actual = date("Y-m-d");
    				if ($fechasig == $fecha_actual) {
    					return "null";
    				}
    				$ok = $this->verificarDisponibilidad($fechasig, $hora, $unidad);
    				if ($ok == 0){
	    				return $fechasig;
    				}
    				else{
    					return "null";
    				}
    			}
    		}

    	}


    }

    public function validacion($fecha, $hora, $unidad){

    	$okey= true;
    	if (($unidad < 1) or ($unidad > 6)) {
    		$okey = false;
    	}
    	else {
    		if (($hora < 10) or ($hora > 21)) {
    			//$okey = false;
    		}
    		else {
    			$fecha_actual = date("Y-m-d");
    			$fecha_inicio = date("Y-m-d",strtotime($fecha_actual."+ 1 days"));
    			$fecha_fin = date("Y-m-d",strtotime($fecha_actual."+ 60 days"));
    			if(($fecha < $fecha_inicio) or ($fecha > $fecha_fin)) {
    				$okey = false;
    			}
    		} 
    	}
    
    	return $okey;

    }


    public function validarUnidad($unidad) {

    	$okey= true;
    	if (($unidad < 1) or ($unidad > 6)) {
    		$okey = false;
    	}

    	return $okey;

    }


    public function registrar(Request $request) {

    	$op = $this->validacion($request->fecha, $request->hora, $request->unidad);

    	if ($op == false){
    		return "Argumentos Incorrectos: Por favor utilize el endpoint '/ayuda' para mas informacion";
    	}

    	else {

    		$ok = $this->verificarDisponibilidad($request->fecha, $request->hora, $request->unidad);
    		if ($ok == 1) {
    			$json = array("fecha" => $request->fecha, "hora" => $request->hora, "unidad" => $request->unidad, "valor" => "false");
    			$var = json_encode($json);
    			return $var;
    		}

    		else {
    			$registro_nuevo = new App\Registro;
    			$registro_nuevo->unidad_id = $request->unidad;
    			$registro_nuevo->fecha = $request->fecha;
    			$registro_nuevo->hora = $request->hora;
    			$registro_nuevo->save();
    			$json = array("fecha" => $request->fecha, "hora" => $request->hora, "unidad" => $request->unidad, "valor" => "true");
    			$var = json_encode($json);
    			return $var;
    		}
    	}

    }

    public function ayuda() {

        $endpoints =App\EndPoint::select('nombre', 'descripcion')
                   ->get();

        return response()->json($endpoints);
    }


    public function buscarUnidades($unidad) {

    	$op = $this->validarUnidad($unidad);

    	if ($op == false) {

    		return "Argumentos Incorrectos: Por favor utilize el endpoint '/ayuda' para mas informacion";
    	}
    	else {
    	
    			if ($unidad == 1) {
    				$results =App\Unidad::select('nro_unidad', 'nombre')->where('nro_unidad', '>', 1)->where('nro_unidad', '<', 4)->get();
    			}

    			if ($unidad == 2) {
    				$results =App\Unidad::select('nro_unidad', 'nombre')->where('nro_unidad', '<', 4)->where('nro_unidad', '<>', 2)->get();
    			}

    			if ($unidad == 3) {
    				$results =App\Unidad::select('nro_unidad', 'nombre')->where('nro_unidad', '<', 4)->where('nro_unidad', '<>', 3)->get();
    			}

    			if ($unidad == 4) {
    				$results =App\Unidad::select('nro_unidad', 'nombre')->where('nro_unidad', '>', 4)->get();
    			}

    			if ($unidad == 5) {
    				$results =App\Unidad::select('nro_unidad', 'nombre')->where('nro_unidad', '>', 3)->where('nro_unidad', '<>', 5)->get();
    			}

    			if ($unidad == 6) {
    				$results =App\Unidad::select('nro_unidad', 'nombre')->where('nro_unidad', '>', 3)->where('nro_unidad', '<>', 6)->get();
    			}

    			return response()->json($results);

    		}	

    }



}
